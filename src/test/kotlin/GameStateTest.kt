import org.junit.Test
import org.junit.jupiter.api.Assertions.*
import kotlin.test.fail

internal class GameStateTest {

    @Test
    fun createNewGameState() {
        GameState.newGame()
    }

    @Test
    fun moveAPawn() {
        val game = GameState.newGame().getPiece(BoardPosition(4, 1))?.moveTo(BoardPosition(4, 2))

        assertEquals(Player.Black, game?.currentPlayer)

        val piece = game?.getPiece(BoardPosition(4, 2))
        piece?.let {
            if (it is Pawn) {
                assertEquals(true, it.moved, "moved pawn should say it is moved")
            }
        } ?: fail()
    }

    @Test
    fun moveAPawnAndCheckUnmoved() {
        val game = GameState.newGame().getPiece(BoardPosition(4, 1))?.moveTo(BoardPosition(4, 2))

        val untouched = game?.getPiece(BoardPosition(5, 1))
        untouched?.let {
            if (it is Pawn) {
                assertEquals(false, it.moved, "untouched pawns should not act as moved")
            }
        } ?: fail()
    }

    @Test
    fun moveABlackPawn() {
        val game: GameState? = GameState.newGame()
                .getPiece(BoardPosition(4, 1))
                ?.moveTo(BoardPosition(4, 2)) //make white move so black can
                ?.getPiece(BoardPosition(4, 6))
                ?.moveTo(BoardPosition(4, 5))

        val piece = game?.getPiece(BoardPosition(4, 5))
        piece?.let {
            if (it is Pawn) {
                assertEquals(true, it.moved, "moved pawn should say it is moved")
            }
        } ?: fail()
    }

    @Test
    fun moveAPawn2() {
        val game = GameState.newGame().getPiece(BoardPosition(4, 1))?.moveTo(BoardPosition(4, 3))

        assertEquals(Player.Black, game?.currentPlayer)

        val piece = game?.getPiece(BoardPosition(4, 3))
        piece?.let {
            if (it is Pawn) {
                assertEquals(true, it.moved, "moved pawn should say it is moved")
            }
        } ?: fail()
    }

    @Test
    fun moveAPawn2after2() {
        val game = GameState.newGame()
            .getPiece(BoardPosition(4, 1))
            ?.moveTo(BoardPosition(4, 3))
//                back move
            ?.getPiece(BoardPosition(4, 6))
            ?.moveTo(BoardPosition(4, 5))
//                second white move
            ?.getPiece(BoardPosition(4, 3))
            ?.moveTo(BoardPosition(4, 5))

        assertNull(game)
    }

    @Test
    fun moveAPawn1After2() {
        val game = GameState.newGame()
            .getPiece(BoardPosition(4, 1))
            ?.moveTo(BoardPosition(4, 3))
//          black move
            ?.getPiece(BoardPosition(4, 6))
            ?.moveTo(BoardPosition(4, 5))
//          second white move
            ?.getPiece(BoardPosition(4, 3))
            ?.moveTo(BoardPosition(4, 4))

        assertEquals(Player.Black, game?.currentPlayer)

        val piece = game?.getPiece(BoardPosition(4, 4))
        piece?.let {
            if (it is Pawn) {
                assertEquals(true, it.moved, "moved pawn should say it is moved")
            }
        } ?: fail()
    }

    @Test
    fun workingOnOurKnightMoves() {
        val game = GameState.newGame()
            .getPiece(BoardPosition(1, 0))
            ?.moveTo(BoardPosition(2,2))

        game?.getPiece(BoardPosition(2, 2))?.let {
            if (it !is Knight) {
                fail()
            }
        } ?: fail()
    }

}