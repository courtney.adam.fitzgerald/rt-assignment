import org.junit.Test
import kotlin.test.assertEquals

internal class MainKtTest {

    @Test
    fun wordsToNumbersExample1() {
        assertEquals("025784", wordsToNumbers("zero;two;five;seven;eight;four"))
    }

    @Test
    fun wordsToNumbersExample2() {
        assertEquals("37892", wordsToNumbers("three;seven;eight;nine;two"))
    }

    @Test
    fun wordsToNumbersEmpty() {
        assertEquals("", wordsToNumbers(""))
    }

    @Test
    fun wordsToNumbersMissing() {
        assertEquals("02784", wordsToNumbers("zero;two;;seven;eight;four"))
    }

    @Test
    fun wordsToNumbersGarbage() {
        assertEquals("", wordsToNumbers("epkjfewkfrwpnfowrnvip;defnowelifn"))
    }

    @Test
    fun wordsToNumbersLessGarbage() {
        assertEquals("02784", wordsToNumbers("zero;two;lehfoien;seven;eight;four"))
    }

    @Test
    fun wordsToNumbersMaxExpecte() {
        assertEquals("00010203040506070809", wordsToNumbers("zero;zero;zero;one;zero;two;zero;three;zero;four;zero;five;zero;six;zero;seven;zero;eight;zero;nine"))
    }

    @Test
    fun pangramDeviancyExample1(){
        assertEquals("NULL", pangramDeviancy("A quick brown fox jumps over the lazy dog."))
    }

    @Test
    fun pangramDeviancyExample2(){
        assertEquals("bjkmqz", pangramDeviancy("A slow yellow fox crawls under the proactive dog."))
    }

    @Test
    fun pangramDeviancyEmpty(){
        assertEquals(('a'..'z').joinToString(""), pangramDeviancy(""))
    }

    @Test
    fun pangramDeviancyExtras(){
        assertEquals("bjkmqz", pangramDeviancy(
            "A slow yellow fox crawls under the proactive dog." +
                "A slow yellow fox crawls under the proactive dog."))
    }
}