import kotlin.math.abs

/**
 * I know it's lacking documentation and full testing but this should be enough to get a feel for the architecture.
 * Which I think is the core of this exercise.
 *
 * The primary interface is via GameState. Any mutation to GameState returns a new GameState or null if in error.
 *
 * basic operation looks like this
 * GameState.newGame() // creates a new game
 * .getPiece(BoardPosition(4, 1)) //gets the piece at the specified coordinates
 * ?.moveTo(BoardPosition(4, 2)) // modifies the game by returning a new GameState
 *
 * Each GameState is immutable so you cannot reuse pieces between GameStates.
 * If you were to take two pieces from the same game state and move each
 * it would create 2 totally independent game states.
 *
 * Castling is not implemented but look to how the pawns double move is implemented to see how that could be achieved.
 * Ditto pawn promotion.
 *
 */

data class BoardPosition(val column: Int, val row: Int) {
    init {
        require(column >= 0)
        require(row >= 0)
        require(column < 8)
        require(row < 8)
    }
}

sealed class Player(val name: String) {
    object White : Player("White")
    object Black : Player("Black")

    override fun toString(): String {
        return "Player(name='$name')"
    }

}

abstract class Piece(val owner: Player, val position: BoardPosition) {

    lateinit var gameState: GameState

    abstract fun validMove(newPosition: BoardPosition): Boolean
    abstract fun duplicate(boardPosition: BoardPosition = position): Piece

    open fun moveTo(boardPosition: BoardPosition): GameState? {
        return if (validMove(boardPosition) && owner == gameState.currentPlayer) {

            //all but me
            var pieces = gameState.pieces.minus(this)
            val captured = gameState.getPiece(boardPosition)
            if (captured != null) {
                // we captured a piece
                pieces = pieces.minus(captured)
            }
            // put the new me in my new place
            pieces = pieces.plus(duplicate(boardPosition))

            GameState(gameState.nextPlayer(), pieces)
        } else {
            null
        }
    }

    override fun toString(): String {
        return "Piece(owner=$owner, position=$position, piece=${javaClass.name})"
    }


}

class Rook(owner: Player, position: BoardPosition) : Piece(owner, position) {
    override fun validMove(newPosition: BoardPosition): Boolean {
        return (newPosition.column == position.column || newPosition.row == position.row) &&
                (newPosition != position)
    }

    override fun duplicate(boardPosition: BoardPosition): Piece {
        return Rook(owner, boardPosition)
    }
}

class Knight(owner: Player, position: BoardPosition) : Piece(owner, position) {
    override fun validMove(newPosition: BoardPosition): Boolean {
        val possibleMoves = listOfNotNull(
            makePosition(2, 1),
            makePosition(2, -1),

            makePosition(-2, 1),
            makePosition(-2, -1),

            makePosition(1, 2),
            makePosition(-1, 2),

            makePosition(1, -2),
            makePosition(-1, -2),
        )
        return possibleMoves.contains(newPosition)
    }

    private fun makePosition(columnOffset: Int, rowOffset: Int): BoardPosition? {
        return try {
            BoardPosition(position.column + columnOffset, position.row + rowOffset)
        } catch (e: IllegalArgumentException) {
            null
        }
    }

    override fun duplicate(boardPosition: BoardPosition): Piece {
        return Knight(owner, boardPosition)
    }

}

class Bishop(owner: Player, position: BoardPosition) : Piece(owner, position) {
    override fun validMove(newPosition: BoardPosition): Boolean {
        return (abs(newPosition.row - position.row) == abs(newPosition.column - newPosition.column))
                && (newPosition != position)
    }

    override fun duplicate(boardPosition: BoardPosition): Piece {
        return Bishop(owner, boardPosition)
    }

}

class Queen(owner: Player, position: BoardPosition) : Piece(owner, position) {
    override fun validMove(newPosition: BoardPosition): Boolean {
        return (newPosition != position) &&
                ((newPosition.column == position.column || newPosition.row == position.row) ||
                        abs(newPosition.row - position.row) == abs(newPosition.column - newPosition.column))
    }

    override fun duplicate(boardPosition: BoardPosition): Piece {
        return Queen(owner, boardPosition)
    }
}

class King(owner: Player, position: BoardPosition) : Piece(owner, position) {
    override fun validMove(newPosition: BoardPosition): Boolean {
        return abs(newPosition.row - position.row) <= 1 &&
                abs(newPosition.column - position.column) <= 1 &&
                newPosition != position
    }

    override fun duplicate(boardPosition: BoardPosition): Piece {
        return King(owner, boardPosition)
    }
}

class Pawn(owner: Player, position: BoardPosition, val moved: Boolean) : Piece(owner, position) {
    override fun validMove(newPosition: BoardPosition): Boolean {
        return gameState.getPiece(newPosition)?.let {
            // could be a capture?
            if (owner == Player.White) {
                newPosition.row == position.row + 1 &&
                        ((newPosition.column == position.column + 1) ||
                                (newPosition.column == position.column - 1))
            } else {
                newPosition.row == position.row - 1 &&
                        ((newPosition.column == position.column + 1) ||
                                (newPosition.column == position.column - 1))
            }

        } ?: basicCheck(newPosition)
    }

    private fun basicCheck(newPosition: BoardPosition): Boolean {
        return (newPosition != position) &&
                (newPosition.column == position.column) &&
                moveDistanceCheck(newPosition.row - position.row)
    }

    private fun moveDistanceCheck(move: Int): Boolean {
        return if (owner == Player.White) {
            allowedDistance() >= move && move > 0
        } else {
            (allowedDistance() * -1) <= move && move < 0
        }
    }

    private fun allowedDistance(): Int {
        return if (moved) {
            1
        } else {
            2
        }
    }

    override fun duplicate(boardPosition: BoardPosition): Piece {
        return Pawn(owner, boardPosition, moved)
    }

    override fun moveTo(boardPosition: BoardPosition): GameState? {
        val gameState = super.moveTo(boardPosition)
        return gameState?.let {
            //mostly correct but moved state for me is wrong
            gameState.getPiece(boardPosition)?.let { newMe ->
                GameState(gameState.currentPlayer, gameState.pieces.minus(newMe).plus(Pawn(owner, boardPosition, true)))
            }
        }
    }
}

class GameState(val currentPlayer: Player, setPieces: List<Piece>) {
    companion object Factory {
        fun newGame(): GameState {
            val pieces = listOf(
                //white back row
                Rook(Player.White, BoardPosition(0, 0)),
                Knight(Player.White, BoardPosition(1, 0)),
                Bishop(Player.White, BoardPosition(2, 0)),
                Queen(Player.White, BoardPosition(3, 0)),
                King(Player.White, BoardPosition(4, 0)),
                Bishop(Player.White, BoardPosition(5, 0)),
                Knight(Player.White, BoardPosition(6, 0)),
                Rook(Player.White, BoardPosition(7, 0)),
                //white front row
                Pawn(Player.White, BoardPosition(0, 1), false),
                Pawn(Player.White, BoardPosition(1, 1), false),
                Pawn(Player.White, BoardPosition(2, 1), false),
                Pawn(Player.White, BoardPosition(3, 1), false),
                Pawn(Player.White, BoardPosition(4, 1), false),
                Pawn(Player.White, BoardPosition(5, 1), false),
                Pawn(Player.White, BoardPosition(6, 1), false),
                Pawn(Player.White, BoardPosition(7, 1), false),
                //black back row
                Rook(Player.Black, BoardPosition(0, 7)),
                Knight(Player.Black, BoardPosition(1, 7)),
                Bishop(Player.Black, BoardPosition(2, 7)),
                Queen(Player.Black, BoardPosition(3, 7)),
                King(Player.Black, BoardPosition(4, 7)),
                Bishop(Player.Black, BoardPosition(5, 7)),
                Knight(Player.Black, BoardPosition(6, 7)),
                Rook(Player.Black, BoardPosition(7, 7)),
                //black front row
                Pawn(Player.Black, BoardPosition(0, 6), false),
                Pawn(Player.Black, BoardPosition(1, 6), false),
                Pawn(Player.Black, BoardPosition(2, 6), false),
                Pawn(Player.Black, BoardPosition(3, 6), false),
                Pawn(Player.Black, BoardPosition(4, 6), false),
                Pawn(Player.Black, BoardPosition(5, 6), false),
                Pawn(Player.Black, BoardPosition(6, 6), false),
                Pawn(Player.Black, BoardPosition(7, 6), false),
            )
            return GameState(Player.White, pieces)
        }
    }

    val pieces: List<Piece> = setPieces.map { it.duplicate() }

    init {
        pieces.forEach {
            it.gameState = this //set initial game state
            val matches = pieces.filter { item -> item.position == it.position }
            require(matches.size == 1 && matches.first() === it) // prevent invalid states
        }
    }

    fun getPiece(boardPosition: BoardPosition): Piece? {
        return pieces.find { it.position == boardPosition }
    }

    fun placePiece( piece: Piece ): GameState{
        var newPieces = pieces
        getPiece(piece.position)?.let{
            newPieces = newPieces.minus(it)
        }
        newPieces.plus(piece)
        return GameState(currentPlayer, newPieces)
    }

    fun nextPlayer(): Player {
        return if (currentPlayer == Player.White) {
            Player.Black
        } else {
            Player.White
        }
    }

    override fun toString(): String {
        return "GameState(currentPlayer=$currentPlayer, pieces=$pieces)"
    }


}

