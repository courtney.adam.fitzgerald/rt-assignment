fun main(args: Array<String>) {
    println("Hello World!")
}

fun wordsToNumbers(wordNumbers: String): String {
    return wordNumbers.toLowerCase().split(';').map {
        when (it) {
            "zero" -> "0"
            "one" -> "1"
            "two" -> "2"
            "three" -> "3"
            "four" -> "4"
            "five" -> "5"
            "six" -> "6"
            "seven" -> "7"
            "eight" -> "8"
            "nine" -> "9"
            else -> ""
        }
    }.joinToString("")
}

fun pangramDeviancy(sample: String): String {
    val asciiAlphaChars = ('a'..'z').toSet()
    val remaining = asciiAlphaChars.minus(sample.toLowerCase().toSet())
    return if (remaining.isEmpty()) {
        "NULL"
    } else {
        remaining.toList().sorted().joinToString("")
    }
}
